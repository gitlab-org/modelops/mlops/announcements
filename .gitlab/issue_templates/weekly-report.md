## :golf: Overview

Machine Learning Operations (MLOps) aims to automate building, experimenting, testing & evaluation, deployment and management of machine learning models in production reliably and efficiently. For the next few milestones the team will be focused on Model Registry and Deployment. We aiming for Model Registry Beta in 17.0.

## :mega: Completed Last Week

## :dart: Focus for This Week

## :book: MLOps Reading List

## Praise

/assign @mray2020 @kbychu
