import os
import gitlab
import datetime

# GitLab instance URL
gitlab_instance = 'https://gitlab.com'

# GitLab CI job token
job_token = os.environ.get('SCHEDULE_CI_JOB_TOKEN')

# Project ID or Project Path with Namespace
project_id = os.environ.get('CI_PROJECT_ID') or '56607930'

# Create a GitLab instance with the job token
gl = gitlab.Gitlab(gitlab_instance, oauth_token=job_token)
project = gl.projects.get(project_id)

# Template file name
template_path = '.gitlab/issue_templates/weekly-report.md'

template_content = project.files.get(file_path=template_path, ref='main').decode().decode('utf-8')

# Get the current date
today = datetime.date.today()

# Calculate the first day of the current week
first_day = today - datetime.timedelta(days=today.weekday())

# set the title
title = f"MLOps Weekly Report - {first_day.strftime('%Y-%m-%d')}"

# Create a new issue with the template
issue = project.issues.create({'title': title, 'description': template_content})

print(f"Issue created: {issue.web_url}")
